package main

import (
	"strconv"
	"encoding/xml"
	"fmt"
	"github.com/fogleman/gg"
	"io/ioutil"
	"os"
	"strings"
)

type TrackData struct {
	When []string `xml:"when"`
}

type Placemark struct {
	XMLName   xml.Name   `xml:"Placemark"`
	TrackData TrackData  `xml:"http://www.google.com/kml/ext/2.2 Track"`
}

type KML struct {
	XMLName    xml.Name    `xml:"kml"`
	Placemarks []Placemark `xml:"Folder>Folder>Placemark"`
}
func parseCoordinates(coord string) (float64, float64) {
	coord = strings.TrimSpace(coord)
	coords := strings.Split(coord, " ")
	if len(coords) != 2 {
		// Handle error if unable to parse coordinates
		return 0, 0
	}
	x, err := strconv.ParseFloat(coords[0], 64)
	if err != nil {
		// Handle error if unable to parse x coordinate
		return 0, 0
	}
	y, err := strconv.ParseFloat(coords[1], 64)
	if err != nil {
		// Handle error if unable to parse y coordinate
		return 0, 0
	}
	return x, y
}

func main() {
	const (
		width  = 800
		height = 600
	)

	// Check if the file path is provided as a command-line argument
	if len(os.Args) < 2 {
		fmt.Println("Please provide the KML file path as a command-line argument.")
		return
	}

	filePath := os.Args[1]

	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Error opening KML file:", err)
		return
	}
	defer file.Close()

	content, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println("Error reading KML file:", err)
		return
	}

	var kml KML
	err = xml.Unmarshal(content, &kml)
	if err != nil {
		fmt.Println("Error parsing KML file:", err)
		return
	}

	// Extract coordinates from the <when> tags within <gx:Track>
	coordinates := make([]string, 0)
	for _, placemark := range kml.Placemarks {
		for _, when := range placemark.TrackData.When {
			coordinates = append(coordinates, when)
		}
	}

	// Create a new image context with the desired dimensions
	dc := gg.NewContext(width, height)
	dc.SetRGB(1, 1, 1) // Set background color to white

	// Iterate over the extracted coordinates and draw the lines
	for _, coord := range coordinates {
		x, y := parseCoordinates(coord)
		dc.LineTo(x, y)
	}

	// Set line attributes such as color and thickness
	dc.SetRGB(0, 0, 0)   // Set line color to black
	dc.SetLineWidth(2.0) // Set line thickness

	dc.Stroke()

	// Save the resulting line drawing to a file
	outputFilePath := os.Args[2]

	err = dc.SavePNG(outputFilePath)
	if err != nil {
		fmt.Println("Error saving the line drawing:", err)
		return
	}

	fmt.Println("Line drawing saved successfully to", outputFilePath)
}
