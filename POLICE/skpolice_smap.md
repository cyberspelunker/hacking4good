```
+ 104.21.54.147 (sni.cloudflaressl.com, papolice.ca)
  - Tags: cdn
  + Ports:
    - 80 tcp/http? 
    - 443 tcp/https? 
    - 2082 tcp/infowave? 
    - 2083 tcp/radsec? 
    - 2086 tcp/gnunet? 
    - 2087 tcp/eli? 
    - 2096 tcp/nbx-dir? 
    - 8080 tcp/http-alt? 
    - 8443 tcp/pcsync-https? 
    - 8880 tcp/cddbp-alt? 
104.26.10.44
  - Tags: cdn
  + Ports:
    - 80 tcp/http? 
    - 443 tcp/https? 
    - 2082 tcp/infowave? 
    - 2083 tcp/radsec? 
    - 2087 tcp/eli? 
    - 2095 tcp/nbx-ser? 
    - 8080 tcp/http-alt? 
    - 8443 tcp/pcsync-https? 
    - 8880 tcp/cddbp-alt? 

+ 104.26.11.44 (sni.cloudflaressl.com, searchinfluence.com)
  - Tags: cdn
  + Ports:
    - 80 tcp/http? 
    - 443 tcp/https? 
    - 2052 tcp/clearvisn? 
    - 2053 tcp/lot105-ds-upd? 
    - 2082 tcp/infowave? 
    - 2083 tcp/radsec? 
    - 2086 tcp/gnunet? 
    - 2087 tcp/eli? 
    - 2095 tcp/nbx-ser? 
    - 2096 tcp/nbx-dir? 
    - 8080 tcp/http-alt? 
    - 8443 tcp/pcsync-https? 
    - 8880 tcp/cddbp-alt? 

+ 167.129.241.239 (sip.saskatoonlibrary.ca, webconf.saskatoon.ca, sip.saskatoon.ca, sip.police.saskatoon.sk.ca)
  + Ports:
    - 443 tcp/https? 

+ 172.64.80.1 (sni.cloudflaressl.com, madisoncountyfl.com)
  - Tags: cdn
  + Ports:
    - 80 tcp/http? 
    - 443 tcp/https? 
    - 2052 tcp/clearvisn? 
    - 2082 tcp/infowave? 
    - 2083 tcp/radsec? 
    - 2086 tcp/gnunet? 
    - 2087 tcp/eli? 
    - 2096 tcp/nbx-dir? 
    - 8080 tcp/http-alt? 
    - 8443 tcp/pcsync-https? 
    - 8880 tcp/cddbp-alt? 

+ 167.129.241.242 (websrvpoolb.saskatoon.ca, lyncdiscover.police.saskatoon.sk.ca, websrvpoola.saskatoon.ca, websrvdirector.saskatoon.ca, meet.saskatoon.ca, dialin.saskatoon.ca, lync.saskatoon.ca, lyncdiscover.saskatoon.ca)
  + Ports:
    - 443 tcp/https? 
172.67.139.104
  - Tags: cdn
  + Ports:
    - 80 tcp/http? 
    - 443 tcp/https? 
    - 2052 tcp/clearvisn? 
    - 2053 tcp/lot105-ds-upd? 
    - 2082 tcp/infowave? 
    - 2083 tcp/radsec? 
    - 2086 tcp/gnunet? 
    - 2087 tcp/eli? 
    - 2096 tcp/nbx-dir? 
    - 8080 tcp/http-alt? 
    - 8443 tcp/pcsync-https? 
    - 8880 tcp/cddbp-alt? 

+ 173.201.193.133 (email25.secureserver.net, email26.secureserver.net, email16.secureserver.net, email21.secureserver.net, email02.secureserver.net, email28.secureserver.net, email13.secureserver.net, email.europe.secureserver.net, email05.secureserver.net, email01.secureserver.net, email14.secureserver.net, email07.europe.secureserver.net, email23.secureserver.net, email10.secureserver.net, email07.secureserver.net, email22.secureserver.net, email19.asia.secureserver.net, www.email.secureserver.net, email.secureserver.net, email.asia.secureserver.net, email06.secureserver.net, email18.secureserver.net, email09.secureserver.net, email19.secureserver.net, email17.secureserver.net, p3plgemwbe17-v05.prod.phx3.secureserver.net, email04.secureserver.net, email12.secureserver.net, email15.secureserver.net, email03.secureserver.net, email11.secureserver.net, email.email.secureserver.net, email24.secureserver.net, email27.secureserver.net)
  + Ports:
    - 80 tcp/http cpe:/a:apache:http_server
    - 443 tcp/https? 

+ 173.201.193.148 (email24.secureserver.net, email16.secureserver.net, email11.secureserver.net, email22.secureserver.net, email27.secureserver.net, email17.secureserver.net, email06.secureserver.net, email.secureserver.net, www.email.secureserver.net, email07.secureserver.net, email14.secureserver.net, email02.secureserver.net, email.email.secureserver.net, email10.secureserver.net, email21.secureserver.net, email28.secureserver.net, email25.secureserver.net, email03.secureserver.net, email.europe.secureserver.net, email01.secureserver.net, email26.secureserver.net, email05.secureserver.net, email19.asia.secureserver.net, email12.secureserver.net, email07.europe.secureserver.net, email19.secureserver.net, email04.secureserver.net, email18.secureserver.net, email23.secureserver.net, p3plgemwbe18-v05.prod.phx3.secureserver.net, email09.secureserver.net, email.asia.secureserver.net, email13.secureserver.net, email15.secureserver.net)
  + Ports:
    - 80 tcp/http cpe:/a:apache:http_server
    - 443 tcp/https? 

+ 172.67.74.48 (sni.cloudflaressl.com, wysa.io)
  - Tags: cdn
  + Ports:
    - 80 tcp/http? 
    - 443 tcp/https? 
    - 2053 tcp/lot105-ds-upd? 
    - 2083 tcp/radsec? 
    - 2086 tcp/gnunet? 
    - 2087 tcp/eli? 
    - 8080 tcp/http-alt? 
    - 8880 tcp/cddbp-alt? 

+ 173.201.193.20 (email25.secureserver.net, email26.secureserver.net, email16.secureserver.net, email21.secureserver.net, email02.secureserver.net, email28.secureserver.net, email13.secureserver.net, email.europe.secureserver.net, email05.secureserver.net, email01.secureserver.net, email14.secureserver.net, email07.europe.secureserver.net, email23.secureserver.net, email10.secureserver.net, email07.secureserver.net, email22.secureserver.net, email19.asia.secureserver.net, www.email.secureserver.net, email.secureserver.net, email.asia.secureserver.net, email06.secureserver.net, email18.secureserver.net, email09.secureserver.net, email19.secureserver.net, email17.secureserver.net, email04.secureserver.net, email12.secureserver.net, email15.secureserver.net, email03.secureserver.net, email11.secureserver.net, p3plgemwbe16-v05.prod.phx3.secureserver.net, email.email.secureserver.net, email24.secureserver.net, email27.secureserver.net)
  + Ports:
    - 80 tcp/http cpe:/a:apache:http_server
    - 443 tcp/https? 

+ 173.201.193.5 (email24.secureserver.net, email16.secureserver.net, email11.secureserver.net, email22.secureserver.net, email27.secureserver.net, email17.secureserver.net, email06.secureserver.net, email.secureserver.net, www.email.secureserver.net, email07.secureserver.net, email14.secureserver.net, email02.secureserver.net, email.email.secureserver.net, email10.secureserver.net, email21.secureserver.net, email28.secureserver.net, email25.secureserver.net, email03.secureserver.net, email.europe.secureserver.net, email01.secureserver.net, email26.secureserver.net, email05.secureserver.net, email19.asia.secureserver.net, email12.secureserver.net, email07.europe.secureserver.net, email19.secureserver.net, email04.secureserver.net, email18.secureserver.net, email23.secureserver.net, p3plgemwbe15-v05.prod.phx3.secureserver.net, email09.secureserver.net, email.asia.secureserver.net, email13.secureserver.net, email15.secureserver.net)
  + Ports:
    - 80 tcp/http cpe:/a:apache:http_server
    - 443 tcp/https? 

+ 192.230.124.49 (tslimits.com, bcedwards.com, mccann.co.il, saskatoonpolice.ca, distributor.invictawatch.com, www.starke.marketing, www.siman.com, www.lms100usergroup.com, 192.230.124.49.ip.incapdns.net, www.focalaudio.com, www.mccann.co.il, conservativeworld.org, bgrnet.bgr.com.ec, www.merzbranding.com, imperva.com, www.insectfree.com, www.bcedwards.com, www.cldchurchonline.com, www.tslimits.com, focalaudio.com, lms100usergroup.com, starke.marketing, www.conservativeworld.org, www.meadowlarkfilmproductions.com, integration.hays.ch, insectfree.com, meadowlarkfilmproductions.com, virtualtour.belfius-art-collection.be, siman.com, cldchurchonline.com, www.saigon-kitchen.ca, api.staging.onemain.financial, merzbranding.com)
  - Tags: cdn
  + Ports:
    - 53 tcp/domain? 
    - 80 tcp/http? 
    - 81 tcp/hosts2-ns? 
    - 82 tcp/xfer? 
    - 83 tcp/mit-ml-dev? 
    - 84 tcp/ctf? 
    - 88 tcp/kerberos-sec? 
    - 443 tcp/https? 
    - 444 tcp/snpp? 
    - 465 tcp/smtps? 
    - 554 tcp/rtsp? 
    - 587 tcp/submission? 
    - 631 tcp/ipp? 
    - 636 tcp/ldaps? 
    - 1024 tcp/kdm? 
    - 1177 tcp/dkmessenger? 
    - 1234 tcp/search-agent? 
    - 1337 tcp/menandmice-dns? 
    - 1400 tcp/cadkey-tablet? 
    - 1433 tcp/ms-sql-s? 
    - 1521 tcp/ncube-lm? 
    - 1935 tcp/macromedia-fcs? 
    - 2000 tcp/cisco-sccp? 
    - 2082 tcp/infowave? 
    - 2083 tcp/radsec? 
    - 2086 tcp/gnunet? 
    - 2087 tcp/eli? 
    - 2222 tcp/msantipiracy? 
    - 2345 tcp/dbm? 
    - 2480 tcp/powerexchange? 
    - 3000 tcp/hbci? 
    - 3001 tcp/nessus? 
    - 3050 tcp/gds_db? 
    - 3268 tcp/msft-gc? 
    - 3269 tcp/msft-gc-ssl? 
    - 3299 tcp/pdrncs? 
    - 3389 tcp/ms-wbt-server? 
    - 3790 tcp/quickbooksrds? 
    - 4000 tcp/icq? 
    - 4022 tcp/dnox? 
    - 4040 tcp/yo-main? 
    - 4443 tcp/pharos? 
    - 4500 tcp/nat-t-ike? 
    - 4567 tcp/tram? 
    - 4848 tcp/appserv-http? 
    - 4911 tcp 
    - 5000 tcp/upnp? 
    - 5001 tcp/commplex-link? 
    - 5006 tcp/wsm-server? 
    - 5010 tcp/telelpathstart? 
    - 5201 tcp/targus-getdata1? 
    - 5222 tcp/xmpp-client? 
    - 5269 tcp/xmpp-server? 
    - 5555 tcp/rplay? 
    - 5560 tcp/isqlplus? 
    - 5601 tcp/esmagent? 
    - 5672 tcp/amqp? 
    - 5800 tcp/vnc-http? 
    - 5900 tcp/rfb? 
    - 5901 tcp/vnc-1? 
    - 5985 tcp/wsman? 
    - 5986 tcp/wsmans? 
    - 6000 tcp/X11? 
    - 6001 tcp/X11:1? 
    - 6443 tcp/sun-sr-https? 
    - 7001 tcp/afs3-callback? 
    - 7071 tcp/iwg1? 
    - 7443 tcp/oracleas-https? 
    - 7547 tcp/cwmp? 
    - 7548 tcp/tidp? 
    - 7777 tcp/cbt? 
    - 7779 tcp/vstat? 
    - 8000 tcp/irdmi? 
    - 8001 tcp/vcom-tunnel? 
    - 8008 tcp/http-alt? 
    - 8009 tcp/ajp13? 
    - 8010 tcp/xmpp? 
    - 8069 tcp 
    - 8080 tcp/http-alt? 
    - 8083 tcp/us-srv? 
    - 8086 tcp/d-s-n? 
    - 8089 tcp 
    - 8090 tcp/opsmessaging? 
    - 8098 tcp 
    - 8099 tcp 
    - 8112 tcp 
    - 8123 tcp/polipo? 
    - 8126 tcp 
    - 8139 tcp 
    - 8140 tcp/puppet? 
    - 8181 tcp/intermapper? 
    - 8200 tcp/trivnet1? 
    - 8443 tcp/pcsync-https? 
    - 8800 tcp/sunwebadmin? 
    - 8834 tcp/nessus-xmlrpc? 
    - 8880 tcp/cddbp-alt? 
    - 8888 tcp/ddi-udp-1? 
    - 8889 tcp/ddi-udp-2? 
    - 9000 tcp/cslistener? 
    - 9001 tcp/etlservicemgr? 
    - 9009 tcp/pichat? 
    - 9080 tcp/glrpc? 
    - 9090 tcp/websm? 
    - 9091 tcp/xmltec-xmlmail? 
    - 9100 tcp/hp-pdl-datastr? 
    - 9191 tcp/sun-as-jpda? 
    - 9200 tcp/wap-wsp? 
    - 9530 tcp 
    - 9600 tcp/micromuse-ncpw? 
    - 9800 tcp/davsrc? 
    - 9943 tcp 
    - 9999 tcp/distinct? 
    - 10000 tcp/ndmp? 
    - 10134 tcp 
    - 10443 tcp/cirrossp? 
    - 12345 tcp/italk? 
    - 20000 tcp/dnp? 
    - 25001 tcp/icl-twobase2? 
    - 31337 tcp/BackOrifice? 
    - 50000 tcp/ibm-db2? 
    - 50050 tcp 

+ 192.238.12.81 (sods.sk.ca, vdcweb1.vdchosting.net, www.sods.sk.ca)
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:8.5 cpe:/o:microsoft:windows
    - 443 tcp/http cpe:/a:microsoft:asp.net
    - 21 tcp/ftp? 
  - Vulns: CVE-2014-4078

+ 20.200.113.11 (certificate-not-found.jobs2web.com)
  - Tags: cloud
  + Ports:
    - 80 tcp/http? 
    - 443 tcp/https? 

+ 199.83.131.49 (tslimits.com, bcedwards.com, mccann.co.il, saskatoonpolice.ca, distributor.invictawatch.com, www.starke.marketing, www.siman.com, www.lms100usergroup.com, www.focalaudio.com, www.mccann.co.il, conservativeworld.org, bgrnet.bgr.com.ec, www.merzbranding.com, imperva.com, www.insectfree.com, www.bcedwards.com, www.cldchurchonline.com, www.tslimits.com, focalaudio.com, lms100usergroup.com, starke.marketing, www.conservativeworld.org, 199.83.131.49.ip.incapdns.net, www.meadowlarkfilmproductions.com, integration.hays.ch, insectfree.com, meadowlarkfilmproductions.com, virtualtour.belfius-art-collection.be, siman.com, cldchurchonline.com, www.saigon-kitchen.ca, api.staging.onemain.financial, merzbranding.com)
  - OS: Unix
  - Tags: cdn
  + Ports:
    - 8008 tcp/http cpe:/a:apache:http_server:2.4.53
    - 25 tcp/smtp? 
    - 53 tcp/domain? 
    - 80 tcp/http? 
    - 81 tcp/hosts2-ns? 
    - 82 tcp/xfer? 
    - 83 tcp/mit-ml-dev? 
    - 84 tcp/ctf? 
    - 88 tcp/kerberos-sec? 
    - 389 tcp/ldap? 
    - 443 tcp/https? 
    - 444 tcp/snpp? 
    - 465 tcp/smtps? 
    - 554 tcp/rtsp? 
    - 587 tcp/submission? 
    - 631 tcp/ipp? 
    - 636 tcp/ldaps? 
    - 1024 tcp/kdm? 
    - 1177 tcp/dkmessenger? 
    - 1234 tcp/search-agent? 
    - 1337 tcp/menandmice-dns? 
    - 1400 tcp/cadkey-tablet? 
    - 1433 tcp/ms-sql-s? 
    - 1521 tcp/ncube-lm? 
    - 1935 tcp/macromedia-fcs? 
    - 2000 tcp/cisco-sccp? 
    - 2082 tcp/infowave? 
    - 2083 tcp/radsec? 
    - 2087 tcp/eli? 
    - 2222 tcp/msantipiracy? 
    - 2345 tcp/dbm? 
    - 2480 tcp/powerexchange? 
    - 3001 tcp/nessus? 
    - 3050 tcp/gds_db? 
    - 3268 tcp/msft-gc? 
    - 3269 tcp/msft-gc-ssl? 
    - 3299 tcp/pdrncs? 
    - 3389 tcp/ms-wbt-server? 
    - 3790 tcp/quickbooksrds? 
    - 4022 tcp/dnox? 
    - 4040 tcp/yo-main? 
    - 4064 tcp/ice-srouter? 
    - 4443 tcp/pharos? 
    - 4500 tcp/nat-t-ike? 
    - 4567 tcp/tram? 
    - 4848 tcp/appserv-http? 
    - 5000 tcp/upnp? 
    - 5001 tcp/commplex-link? 
    - 5005 tcp/avt-profile-2? 
    - 5007 tcp/wsm-server-ssl? 
    - 5009 tcp/winfs? 
    - 5010 tcp/telelpathstart? 
    - 5201 tcp/targus-getdata1? 
    - 5269 tcp/xmpp-server? 
    - 5555 tcp/rplay? 
    - 5560 tcp/isqlplus? 
    - 5672 tcp/amqp? 
    - 5800 tcp/vnc-http? 
    - 5900 tcp/rfb? 
    - 5901 tcp/vnc-1? 
    - 5985 tcp/wsman? 
    - 5986 tcp/wsmans? 
    - 6000 tcp/X11? 
    - 6001 tcp/X11:1? 
    - 6080 tcp/gue? 
    - 6443 tcp/sun-sr-https? 
    - 7001 tcp/afs3-callback? 
    - 7071 tcp/iwg1? 
    - 7443 tcp/oracleas-https? 
    - 7474 tcp/neo4j? 
    - 7547 tcp/cwmp? 
    - 7548 tcp/tidp? 
    - 7777 tcp/cbt? 
    - 8000 tcp/irdmi? 
    - 8001 tcp/vcom-tunnel? 
    - 8009 tcp/ajp13? 
    - 8010 tcp/xmpp? 
    - 8060 tcp/aero? 
    - 8069 tcp 
    - 8080 tcp/http-alt? 
    - 8081 tcp/sunproxyadmin? 
    - 8086 tcp/d-s-n? 
    - 8089 tcp 
    - 8090 tcp/opsmessaging? 
    - 8123 tcp/polipo? 
    - 8126 tcp 
    - 8139 tcp 
    - 8140 tcp/puppet? 
    - 8200 tcp/trivnet1? 
    - 8443 tcp/pcsync-https? 
    - 8800 tcp/sunwebadmin? 
    - 8834 tcp/nessus-xmlrpc? 
    - 8880 tcp/cddbp-alt? 
    - 8888 tcp/ddi-udp-1? 
    - 8889 tcp/ddi-udp-2? 
    - 9000 tcp/cslistener? 
    - 9001 tcp/etlservicemgr? 
    - 9002 tcp/dynamid? 
    - 9009 tcp/pichat? 
    - 9080 tcp/glrpc? 
    - 9090 tcp/websm? 
    - 9091 tcp/xmltec-xmlmail? 
    - 9100 tcp/hp-pdl-datastr? 
    - 9191 tcp/sun-as-jpda? 
    - 9200 tcp/wap-wsp? 
    - 9306 tcp/sphinxql? 
    - 9443 tcp/tungsten-https? 
    - 9530 tcp 
    - 9800 tcp/davsrc? 
    - 9943 tcp 
    - 9999 tcp/distinct? 
    - 10000 tcp/ndmp? 
    - 10001 tcp/scp-config? 
    - 10134 tcp 
    - 10443 tcp/cirrossp? 
    - 12345 tcp/italk? 
    - 20000 tcp/dnp? 
    - 25001 tcp/icl-twobase2? 
    - 31337 tcp/BackOrifice? 
    - 50000 tcp/ibm-db2? 
    - 50050 tcp 
    - 60001 tcp 
  - Vulns: CVE-2022-30522, CVE-2022-28330, CVE-2022-3602, CVE-2022-2097, CVE-2022-1473, CVE-2022-3786, CVE-2022-31813, CVE-2022-28615, CVE-2022-1434, CVE-2006-20001, CVE-2022-30556, CVE-2022-37436, CVE-2022-3358, CVE-2022-3996, CVE-2022-29404, CVE-2022-1343, CVE-2022-0778, CVE-2022-28614, CVE-2022-1292, CVE-2022-36760, CVE-2022-26377, CVE-2022-2068

+ 192.230.67.49 (tslimits.com, bcedwards.com, mccann.co.il, saskatoonpolice.ca, distributor.invictawatch.com, www.starke.marketing, www.siman.com, www.lms100usergroup.com, www.focalaudio.com, www.mccann.co.il, conservativeworld.org, bgrnet.bgr.com.ec, www.merzbranding.com, imperva.com, www.insectfree.com, www.bcedwards.com, www.cldchurchonline.com, 192.230.67.49.ip.incapdns.net, www.tslimits.com, focalaudio.com, lms100usergroup.com, starke.marketing, www.conservativeworld.org, www.meadowlarkfilmproductions.com, integration.hays.ch, insectfree.com, meadowlarkfilmproductions.com, virtualtour.belfius-art-collection.be, siman.com, cldchurchonline.com, www.saigon-kitchen.ca, api.staging.onemain.financial, merzbranding.com)
  - Tags: cdn
  + Ports:
    - 80 tcp/http cpe:/a:apache:http_server:2.4.34
    - 82 tcp/http cpe:/a:php:php:8.1.8
    - 81 tcp/http cpe:/a:openssl:openssl:1.0.2k
    - 25 tcp/smtp? 
    - 53 tcp/domain? 
    - 84 tcp/ctf? 
    - 88 tcp/kerberos-sec? 
    - 443 tcp/https? 
    - 444 tcp/snpp? 
    - 465 tcp/smtps? 
    - 554 tcp/rtsp? 
    - 587 tcp/submission? 
    - 631 tcp/ipp? 
    - 636 tcp/ldaps? 
    - 1024 tcp/kdm? 
    - 1177 tcp/dkmessenger? 
    - 1234 tcp/search-agent? 
    - 1337 tcp/menandmice-dns? 
    - 1400 tcp/cadkey-tablet? 
    - 1433 tcp/ms-sql-s? 
    - 1521 tcp/ncube-lm? 
    - 1935 tcp/macromedia-fcs? 
    - 2082 tcp/infowave? 
    - 2083 tcp/radsec? 
    - 2086 tcp/gnunet? 
    - 2087 tcp/eli? 
    - 2345 tcp/dbm? 
    - 3000 tcp/hbci? 
    - 3001 tcp/nessus? 
    - 3050 tcp/gds_db? 
    - 3268 tcp/msft-gc? 
    - 3269 tcp/msft-gc-ssl? 
    - 3389 tcp/ms-wbt-server? 
    - 3790 tcp/quickbooksrds? 
    - 4000 tcp/icq? 
    - 4064 tcp/ice-srouter? 
    - 4443 tcp/pharos? 
    - 4500 tcp/nat-t-ike? 
    - 4567 tcp/tram? 
    - 4848 tcp/appserv-http? 
    - 4911 tcp 
    - 5000 tcp/upnp? 
    - 5001 tcp/commplex-link? 
    - 5005 tcp/avt-profile-2? 
    - 5006 tcp/wsm-server? 
    - 5007 tcp/wsm-server-ssl? 
    - 5009 tcp/winfs? 
    - 5010 tcp/telelpathstart? 
    - 5201 tcp/targus-getdata1? 
    - 5222 tcp/xmpp-client? 
    - 5269 tcp/xmpp-server? 
    - 5555 tcp/rplay? 
    - 5601 tcp/esmagent? 
    - 5672 tcp/amqp? 
    - 5800 tcp/vnc-http? 
    - 5901 tcp/vnc-1? 
    - 5985 tcp/wsman? 
    - 5986 tcp/wsmans? 
    - 6080 tcp/gue? 
    - 6443 tcp/sun-sr-https? 
    - 7001 tcp/afs3-callback? 
    - 7071 tcp/iwg1? 
    - 7171 tcp/drm-production? 
    - 7443 tcp/oracleas-https? 
    - 7474 tcp/neo4j? 
    - 7547 tcp/cwmp? 
    - 7548 tcp/tidp? 
    - 7777 tcp/cbt? 
    - 8000 tcp/irdmi? 
    - 8001 tcp/vcom-tunnel? 
    - 8009 tcp/ajp13? 
    - 8010 tcp/xmpp? 
    - 8060 tcp/aero? 
    - 8069 tcp 
    - 8080 tcp/http-alt? 
    - 8081 tcp/sunproxyadmin? 
    - 8083 tcp/us-srv? 
    - 8086 tcp/d-s-n? 
    - 8089 tcp 
    - 8090 tcp/opsmessaging? 
    - 8098 tcp 
    - 8112 tcp 
    - 8123 tcp/polipo? 
    - 8126 tcp 
    - 8140 tcp/puppet? 
    - 8181 tcp/intermapper? 
    - 8200 tcp/trivnet1? 
    - 8443 tcp/pcsync-https? 
    - 8834 tcp/nessus-xmlrpc? 
    - 8880 tcp/cddbp-alt? 
    - 8888 tcp/ddi-udp-1? 
    - 9000 tcp/cslistener? 
    - 9001 tcp/etlservicemgr? 
    - 9002 tcp/dynamid? 
    - 9009 tcp/pichat? 
    - 9080 tcp/glrpc? 
    - 9090 tcp/websm? 
    - 9091 tcp/xmltec-xmlmail? 
    - 9100 tcp/hp-pdl-datastr? 
    - 9200 tcp/wap-wsp? 
    - 9306 tcp/sphinxql? 
    - 9443 tcp/tungsten-https? 
    - 9530 tcp 
    - 9600 tcp/micromuse-ncpw? 
    - 9943 tcp 
    - 9999 tcp/distinct? 
    - 10000 tcp/ndmp? 
    - 10001 tcp/scp-config? 
    - 10134 tcp 
    - 10443 tcp/cirrossp? 
    - 12345 tcp/italk? 
    - 20000 tcp/dnp? 
    - 25001 tcp/icl-twobase2? 
    - 31337 tcp/BackOrifice? 
    - 50000 tcp/ibm-db2? 
    - 50050 tcp 
    - 60001 tcp 
  - Vulns: CVE-2019-10081, CVE-2022-22721, CVE-2020-1927, CVE-2022-2097, CVE-2022-1473, CVE-2017-3736, CVE-2022-31813, CVE-2019-1559, CVE-2021-3712, CVE-2021-26691, CVE-2022-29404, CVE-2019-0211, CVE-2022-22720, CVE-2019-10082, CVE-2019-10092, CVE-2022-1292, CVE-2017-3737, CVE-2021-23840, CVE-2018-0739, CVE-2019-1563, CVE-2020-35452, CVE-2022-30522, CVE-2022-28330, CVE-2021-23841, CVE-2019-0197, CVE-2022-28615, CVE-2020-13938, CVE-2021-36160, CVE-2021-44224, CVE-2022-30556, CVE-2019-0220, CVE-2020-11984, CVE-2018-0732, CVE-2020-1971, CVE-2018-11763, CVE-2022-0778, CVE-2017-3738, CVE-2021-33193, CVE-2019-0217, CVE-2019-10097, CVE-2017-3735, CVE-2022-3602, CVE-2020-11993, CVE-2019-1547, CVE-2019-1552, CVE-2022-1434, CVE-2022-23943, CVE-2018-17189, CVE-2021-26690, CVE-2018-17199, CVE-2019-0196, CVE-2022-37436, CVE-2020-1968, CVE-2022-3996, CVE-2021-34798, CVE-2022-1343, CVE-2019-17567, CVE-2022-28614, CVE-2018-5407, CVE-2022-2068, CVE-2020-9490, CVE-2018-0737, CVE-2021-4160, CVE-2022-3786, CVE-2019-1551, CVE-2021-44790, CVE-2019-10098, CVE-2006-20001, CVE-2019-9517, CVE-2021-39275, CVE-2018-0734, CVE-2022-3358, CVE-2021-40438, CVE-2020-1934, CVE-2022-36760, CVE-2022-26377, CVE-2022-22719

+ 207.195.35.66 (www.papolice.ca, www.PAPSASA.papolice.ca, 207-195-35-66.prna.static.sasknet.sk.ca, papsutm.papolice.ca, www.mail.papolice.ca, PAPSASA.papolice.ca, autodiscover.papolice.ca, papolice.ca, mail.papolice.ca)
  - Tags: starttls
  + Ports:
    - 443 tcp/http cpe:/a:microsoft:asp.net
    - 25 tcp/smtp? 
    - 4443 tcp/pharos? 

+ 207.195.36.124 (police.regina.sk.ca)
  + Ports:
    - 80 tcp/http? 

+ 35.161.163.24 (ec2-35-161-163-24.us-west-2.compute.amazonaws.com)
  - Tags: cloud
  + Ports:
    - 80 tcp/http cpe:/a:igor_sysoev:nginx

+ 216.7.89.29 (ayjor359.hostpapavps.net, weyburnpolice.ca)
  - OS: MiniBSD
  - Tags: starttls
  + Ports:
    - 21 tcp/ftp cpe:/a:pureftpd:pure-ftpd
    - 465 tcp/smtp cpe:/a:exim:exim:4.95
    - 80 tcp/http cpe:/a:php:php
    - 22 tcp/ssh? 
    - 110 tcp/pop3? 
    - 143 tcp/imap? 
    - 443 tcp/https? 
    - 587 tcp/submission? 
    - 993 tcp/imaps? 
    - 995 tcp/pop3s? 
    - 2079 tcp/idware-router? 
    - 2083 tcp/radsec? 
    - 2086 tcp/gnunet? 
    - 2087 tcp/eli? 
    - 2095 tcp/nbx-ser? 
    - 2096 tcp/nbx-dir? 
  - Vulns: CVE-2018-15919, CVE-2019-6109, CVE-2019-6111, CVE-2019-6110, CVE-2021-41617, CVE-2020-14145, CVE-2018-20685, CVE-2018-15473, CVE-2020-15778, CVE-2021-36368, CVE-2017-15906, CVE-2016-20012

+ 35.208.26.193 (www.cbxaerobeam.com, 193.26.208.35.bc.googleusercontent.com, cbxaerobeam.com, giowm1051.siteground.biz)
  - Tags: cloud, starttls, database
  + Ports:
    - 21 tcp/ftp cpe:/a:pureftpd:pure-ftpd
    - 3306 tcp/mysql cpe:/a:mysql:mysql
    - 5432 tcp/postgresql cpe:/a:postgresql:postgresql:10
    - 80 tcp/http cpe:/a:php:php
    - 25 tcp/smtp? 
    - 110 tcp/pop3? 
    - 443 tcp/https? 
    - 465 tcp/smtps? 
    - 587 tcp/submission? 
    - 993 tcp/imaps? 
    - 995 tcp/pop3s? 
    - 2525 tcp/ms-v-worlds? 
  - Vulns: CVE-2017-12172, CVE-2017-15098

+ 45.40.130.40 (email24.secureserver.net, email16.secureserver.net, email11.secureserver.net, email22.secureserver.net, email27.secureserver.net, email17.secureserver.net, email06.secureserver.net, email.secureserver.net, www.email.secureserver.net, email07.secureserver.net, email14.secureserver.net, email02.secureserver.net, email.email.secureserver.net, email10.secureserver.net, email21.secureserver.net, email28.secureserver.net, email25.secureserver.net, email03.secureserver.net, email.europe.secureserver.net, email01.secureserver.net, p3plgemwbe26-v05.prod.phx3.secureserver.net, email26.secureserver.net, email05.secureserver.net, email19.asia.secureserver.net, email12.secureserver.net, email07.europe.secureserver.net, email19.secureserver.net, email04.secureserver.net, email18.secureserver.net, email23.secureserver.net, email09.secureserver.net, email.asia.secureserver.net, email13.secureserver.net, email15.secureserver.net)
  + Ports:
    - 80 tcp/http cpe:/a:apache:http_server
    - 443 tcp/https? 
40.97.205.24
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows

+ 45.40.140.6 (email24.secureserver.net, email16.secureserver.net, email11.secureserver.net, email22.secureserver.net, email27.secureserver.net, email17.secureserver.net, email06.secureserver.net, email.secureserver.net, www.email.secureserver.net, email07.secureserver.net, email14.secureserver.net, email02.secureserver.net, email.email.secureserver.net, email10.secureserver.net, email21.secureserver.net, email28.secureserver.net, email25.secureserver.net, email03.secureserver.net, email.europe.secureserver.net, email01.secureserver.net, email26.secureserver.net, email05.secureserver.net, email19.asia.secureserver.net, email12.secureserver.net, email07.europe.secureserver.net, email19.secureserver.net, email04.secureserver.net, email18.secureserver.net, email23.secureserver.net, p3plgemwbe27-v05.prod.phx3.secureserver.net, email09.secureserver.net, email.asia.secureserver.net, email13.secureserver.net, email15.secureserver.net)
  + Ports:
    - 80 tcp/http cpe:/a:apache:http_server
    - 443 tcp/https? 

+ 45.40.130.41 (email24.secureserver.net, email16.secureserver.net, email11.secureserver.net, email22.secureserver.net, email27.secureserver.net, email17.secureserver.net, email06.secureserver.net, email.secureserver.net, www.email.secureserver.net, email07.secureserver.net, email14.secureserver.net, email02.secureserver.net, email.email.secureserver.net, email10.secureserver.net, email21.secureserver.net, email28.secureserver.net, email25.secureserver.net, email03.secureserver.net, email.europe.secureserver.net, email01.secureserver.net, email26.secureserver.net, email05.secureserver.net, p3plgemwbe25-v05.prod.phx3.secureserver.net, email19.asia.secureserver.net, email12.secureserver.net, email07.europe.secureserver.net, email19.secureserver.net, email04.secureserver.net, email18.secureserver.net, email23.secureserver.net, email09.secureserver.net, email.asia.secureserver.net, email13.secureserver.net, email15.secureserver.net)
  + Ports:
    - 80 tcp/http cpe:/a:apache:http_server
    - 443 tcp/https? 

+ 50.62.160.97 (p3nwvpweb057.shr.prod.phx3.secureserver.net, usenginevalve.com, shr.prod.phx3.secureserver.net, www.usenginevalve.com)
  - OS: Windows
  - Tags: starttls
  + Ports:
    - 80 tcp/http cpe:/a:php:php
    - 21 tcp/ftp? 
    - 443 tcp/https? 
    - 8443 tcp/pcsync-https? 
52.96.113.200
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.110.24
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.121.24
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.113.248
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.166.136
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.166.216
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.166.8
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.18.184
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.190.200
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.230.152
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.230.56
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.51.104
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.59.232
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows
52.96.70.136
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows

+ 68.178.252.20 (email24.secureserver.net, email16.secureserver.net, email11.secureserver.net, email22.secureserver.net, email27.secureserver.net, email17.secureserver.net, email06.secureserver.net, email.secureserver.net, www.email.secureserver.net, email07.secureserver.net, email14.secureserver.net, email02.secureserver.net, email.email.secureserver.net, email10.secureserver.net, email21.secureserver.net, email28.secureserver.net, email25.secureserver.net, email03.secureserver.net, email.europe.secureserver.net, email01.secureserver.net, email26.secureserver.net, email05.secureserver.net, p3plgemwbe22-v05.prod.phx3.secureserver.net, email19.asia.secureserver.net, email12.secureserver.net, email07.europe.secureserver.net, email19.secureserver.net, email04.secureserver.net, email18.secureserver.net, email23.secureserver.net, email09.secureserver.net, email.asia.secureserver.net, email13.secureserver.net, email15.secureserver.net)
  + Ports:
    - 80 tcp/http cpe:/a:apache:http_server
    - 443 tcp/https? 
52.96.88.184
  - OS: Windows
  + Ports:
    - 80 tcp/http cpe:/a:microsoft:internet_information_services:10.0 cpe:/o:microsoft:windows

+ 68.178.252.5 (email24.secureserver.net, email16.secureserver.net, email11.secureserver.net, email22.secureserver.net, email27.secureserver.net, email17.secureserver.net, email06.secureserver.net, email.secureserver.net, www.email.secureserver.net, email07.secureserver.net, email14.secureserver.net, email02.secureserver.net, email.email.secureserver.net, email10.secureserver.net, email21.secureserver.net, email28.secureserver.net, email25.secureserver.net, email03.secureserver.net, email.europe.secureserver.net, email01.secureserver.net, email26.secureserver.net, email05.secureserver.net, email19.asia.secureserver.net, email12.secureserver.net, email07.europe.secureserver.net, email19.secureserver.net, email04.secureserver.net, email18.secureserver.net, email23.secureserver.net, email09.secureserver.net, email.asia.secureserver.net, email13.secureserver.net, p3plgemwbe21-v05.prod.phx3.secureserver.net, email15.secureserver.net)
  + Ports:
    - 80 tcp/http cpe:/a:apache:http_server
    - 443 tcp/https? 

+ 69.27.107.132 (vps.reginapolice.ca)
  - Tags: starttls
  + Ports:
    - 21 tcp/ftp cpe:/a:pureftpd:pure-ftpd
    - 80 tcp/http cpe:/a:apache:http_server
    - 443 tcp/https? 

+ 71.17.5.226 (fortimail.reginapolice.ca, 71-17-5-226.sktn.static.sasknet.sk.ca, 71-17-5-226.sktn.hsdb.sasknet.sk.ca)
  + Ports:
    - 25 tcp/smtp? 
    - 179 tcp/bgp? 
    - 443 tcp/https? 
    - 1701 tcp/L2TP? 

+ 71.17.5.232 (71-17-5-232.sktn.hsdb.sasknet.sk.ca, slnk.reginapolice.ca, 71-17-5-232.sktn.static.sasknet.sk.ca)
  + Ports:
    - 22 tcp/ssh? 
    - 80 tcp/http? 
    - 179 tcp/bgp? 
    - 443 tcp/https? 
```
