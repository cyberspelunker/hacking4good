### Weak Cipher Suites Detection (weak-cipher-suites) found on sip.police.saskatoon.sk.ca
---
**Details**: **weak-cipher-suites**  matched at sip.police.saskatoon.sk.ca

**Protocol**: SSL

**Full URL**: sip.police.saskatoon.sk.ca

**Template Information**

| Key | Value |
|---|---|
| Name | Weak Cipher Suites Detection |
| Authors | pussycat0x |
| Tags | ssl, tls, misconfig |
| Severity | medium |
| Description | A weak cipher is defined as an encryption/decryption algorithm that uses a key of insufficient length. Using an insufficient length for a key in an encryption/decryption algorithm opens up the possibility (or probability) that the encryption scheme could be broken. |

**Extra Information**

**Extracted results**:

- TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256


References: 
- https://www.acunetix.com/vulnerabilities/web/tls-ssl-weak-cipher-suites/
- http://ciphersuite.info

---
